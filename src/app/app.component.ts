import { Component, ChangeDetectorRef } from '@angular/core';
import { Platform, NavController, ModalController } from '@ionic/angular';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';
import { UserService } from './services/user/user.service';
import { Router } from '@angular/router';
import { NativePageTransitions, NativeTransitionOptions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { SpacesService } from './services/spaces/spaces.service';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { AlertService } from './services/alert/alert.service';
import { EventsService } from './services/events/events.service';
import { GuideService } from './services/guide/guide.service';
import { FramesService } from './services/frames/frames.service';
import { WssSocketService } from './services/wss-socket/wss-socket.service';
import { ID_APP } from './config/config';
import { SerialesService } from './services/seriales/seriales.service';
import { createAnimation } from '@ionic/core';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { ImageLoaderConfigService } from 'ionic-image-loader';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public showSplash: boolean = true;
  public spaces: Array<any>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public router: Router,
    public spaceService: SpacesService,
    public userService: UserService,
    public eventsService: EventsService,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public nativePageTransitions: NativePageTransitions,
    public oneSignal: OneSignal,
    public alertCtrl: AlertService,
    public guideService: GuideService,
    public framesService: FramesService,
    public serialesService: SerialesService,
    public webSocket: WssSocketService,
    public splashScreen: SplashScreen,
    private cd: ChangeDetectorRef,
    private imageLoaderConfig: ImageLoaderConfigService
  ) {
    this.spaces = this.spaceService.spaces;
    this.spaceService.$spaces.subscribe((spaces) => {
      this.spaces = spaces;
    });
    this.imageLoaderConfig.enableSpinner(false);
    this.initializeApp();
  }

  ngOnInit () {
  }

  initSplashScreen (event?) {
    this.showSplash = true;
    this.splashScreen.hide();
    this.cd.detectChanges();
    this.initAnimation();
    setTimeout(() => {
      this.showSplash = false;
      this.cd.detectChanges();
    }, 3000);
  }

  initAnimation () {
    createAnimation()
      .addElement(document.querySelector('.img-splash'))
      .iterations(1)
      .delay(0)
      .duration(500)
      .direction('normal')
      .easing('ease-in-out')
      .keyframes([
        { offset: 0, opacity: '0' },
        { offset: 1, opacity: '1' }
      ])
      .play();
  }

  initializeApp () {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.handlerNotifications();
      }
      this.initSocket();
      this.userService.isLogged.subscribe((state) => {
        if (state) {
          this.redirectTo(this.userService.user.role);
        } else {
          this.router.navigate(['log-in']);
        }
      });
      this.backButtonHandler();
      this.platform.resume.subscribe(() => {
        this.initSplashScreen();
        this.eventsService.getEvents();
        this.framesService.getFrames();
        this.guideService.getTopics();
        this.serialesService.getSeriales();
        this.spaceService.getSpaces();
      });
    });
  }

  redirectTo (role) {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    switch (role) {
      case 'user':
        this.navCtrl.navigateRoot(['logged-in']);
        break;
      default:
        this.navCtrl.navigateRoot(['log-in']);
        break;
    }
  }

  handlerNotifications () {
    /*this.oneSignal.startInit('d91a7521-140e-4bf3-8207-7e7d9663dc1a', '180249517887');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.getIds().then((identity) => {
      let interval = setInterval(() => {
        if (this.userService.user) {
          this.userService.edit({ player_id: identity.userId, id: this.userService.user.id });
          clearInterval(interval);
        }
      }, 100);
    });
    this.oneSignal.handleNotificationOpened().subscribe((jsonData) => {
      this.notificationRedirect(jsonData.notification.payload.additionalData);
    });
    this.oneSignal.endInit();*/
  }

  notificationRedirect (notificationData) {
    switch (notificationData.target) {
      case 'event':
        this.eventsService.getEvent(notificationData.id).then(() => {
          this.navCtrl.navigateForward(['logged-in', 'tabs', 'events', 'details', notificationData.id]);
        }).catch(() => {
          this.navCtrl.navigateForward(['logged-in', 'tabs', 'events']);
        });
        break;
      case 'events':
        this.navCtrl.navigateForward(['logged-in', 'tabs', 'events']);
        break;
      case 'guide':
        this.navCtrl.navigateForward(['logged-in', 'tabs', 'guide']);
        break;
      default:
        this.navCtrl.navigateForward(['logged-in', 'tabs', 'logged-in']);
        break;
    }
  }

  initSocket () {
    let handler = (message) => {
      if (message.id == ID_APP) {
        if (message.target == 'ole_events') {
          console.log('events update');
          this.eventsService.getEvents();
        }
        if (message.target == 'ole_frames') {
          console.log('events update');
          this.framesService.getFrames();
        }
        if (message.target == 'ole_guide') {
          console.log('events update');
          this.guideService.getTopics();
        }
        if (message.target == 'ole_events') {
          console.log('seriales update');
          this.serialesService.getSeriales();
        }
        if (message.target == 'ole_spaces') {
          console.log('spaces update');
          this.spaceService.getSpaces();
        }
      }
    }

    this.webSocket.setOnMessage(handler);
  }

  backButtonHandler () {
    this.platform.backButton.subscribe(() => {
      console.log(this.router.url);
      if (this.router.url == "logged-in/tabs/home"
        || this.router.url == "logged-in/tabs/events"
        || this.router.url == "logged-in/tabs/phto"
        || this.router.url == "logged-in/tabs/guide") {
        return;
      }
    });
  }
}
