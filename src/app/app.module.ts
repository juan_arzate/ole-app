import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { Camera } from '@awesome-cordova-plugins/camera/ngx';
import { FileTransfer, FileTransferObject } from '@awesome-cordova-plugins/file-transfer/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ComponentsModule } from './components/components.module';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { Facebook } from '@awesome-cordova-plugins/facebook/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import {SignInWithApple} from '@awesome-cordova-plugins/sign-in-with-apple/ngx';

import { LoaderService } from './services/loader/loader.service';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IonicModule.forRoot({
      mode: 'ios',
      animated: false,
      hardwareBackButton: true
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    ComponentsModule,
    IonicImageLoader.forRoot(),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions,
    InAppBrowser,
    Camera,
    FileTransfer,
    FileTransferObject,
    File,
    Base64ToGallery,
    WebView,
    SocialSharing,
    OneSignal,
    Facebook,
    LoaderService,
    AndroidPermissions,
    SignInWithApple,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
