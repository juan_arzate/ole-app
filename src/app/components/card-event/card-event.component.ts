import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { EventsService } from 'src/app/services/events/events.service';

@Component({
  selector: 'card-event',
  templateUrl: './card-event.component.html',
  styleUrls: ['./card-event.component.scss'],
})
export class CardEventComponent implements OnInit {

  @ViewChild('img', { static: false }) img: ElementRef;
  @Input() source: string;
  @Input() event: any;

  public isFavorite = false;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public eventService: EventsService,
    public toastCtrl: ToastController,
  ) {
  }

  ngOnInit() {
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    this.eventService.isFavorite(this.event.event_id).then((isFavorite) => {
      this.isFavorite = isFavorite;
    });
  }

  async toggleFavorite(event: Event) {
    event.stopPropagation();
    if (!this.isFavorite) {
      this.eventService.saveAsFavoriteEvent(this.event.event_id).then((isFavorite) => {
        this.isFavorite = isFavorite;
        this.eventService.getFavoriteEvents();
        if (isFavorite) {
          this.toastCtrl.create({
            buttons: [{
              role: 'cancel',
              icon: 'close',
              side: 'end',
              text: ''
            }],
            position: 'bottom',
            duration: 3000,
            color: 'warning',
            message: 'Se agregó a tus eventos favoritos'
          }).then((a) => { a.present(); });
        }
      });
    } else {
      this.eventService.deleteFromFavoriteEvent(this.event.event_id).then((isFavorite) => {
        this.isFavorite = !isFavorite;
        this.eventService.getFavoriteEvents();
        if (!isFavorite) {
          this.toastCtrl.create({
            buttons: [{
              role: 'cancel',
              icon: 'close',
              side: 'end',
              text: ''
            }],
            position: 'bottom',
            color: 'warning',
            duration: 3000,
            message: 'Se eliminó a tus eventos favoritos'
          }).then((a) => { a.present(); });
        }
      });
    }
  }

  goToEventDetail($event) {
    $event.stopPropagation();
    const options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };
    this.eventService.currentEvent = this.event;
    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'events', 'details', this.event.event_id]);
  }

}
