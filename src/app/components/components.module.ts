import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CardEventComponent } from './card-event/card-event.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { ModalItemDetailsComponent } from './modal-item-details/modal-item-details.component';
import { ToreroCardComponent } from './torero-card/torero-card.component';
import { FrameItemComponent } from './frame-item/frame-item.component';
import { ModalGanaderiaDetailsComponent } from './modal-ganaderia-details/modal-ganaderia-details.component';
import { ModalToreroDetailsComponent } from './modal-torero-details/modal-torero-details.component';
import { GanaderiaCardComponent } from './ganaderia-card/ganaderia-card.component';
import { EventsListComponent } from './events-list/events-list.component';
import { MonthsListComponent } from './months-list/months-list.component';
import localeMX from '@angular/common/locales/es-MX';
import { ToroCardComponent } from './toro-card/toro-card.component';
import { ModalTorosDetailsComponent } from './modal-toros-details/modal-toros-details.component';
import { IonicImageLoader } from 'ionic-image-loader';
import { ModalImageComponent } from './modal-image/modal-image.component';

registerLocaleData(localeMX);

@NgModule({
  declarations: [
    CardEventComponent,
    ItemCardComponent,
    ToreroCardComponent,
    FrameItemComponent,
    ModalItemDetailsComponent,
    ModalGanaderiaDetailsComponent,
    ModalToreroDetailsComponent,
    GanaderiaCardComponent,
    EventsListComponent,
    MonthsListComponent,
    ToroCardComponent,
    ModalTorosDetailsComponent,
    ModalImageComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    IonicImageLoader
  ],
  exports: [
    CardEventComponent,
    ItemCardComponent,
    ToreroCardComponent,
    FrameItemComponent,
    GanaderiaCardComponent,
    EventsListComponent,
    MonthsListComponent,
    ModalTorosDetailsComponent,
    ModalImageComponent
  ],
  entryComponents: [
    ModalItemDetailsComponent,
    ModalGanaderiaDetailsComponent,
    ModalToreroDetailsComponent,
    EventsListComponent,
    MonthsListComponent, 
    ToroCardComponent,
    ModalTorosDetailsComponent,
    ModalImageComponent

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-MX' }
  ]
})
export class ComponentsModule { }
