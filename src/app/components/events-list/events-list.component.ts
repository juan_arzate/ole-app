import { Component, OnInit } from '@angular/core';
import { SerialesService } from 'src/app/services/seriales/seriales.service';
import { EventsService } from 'src/app/services/events/events.service';
import { PopoverController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader/loader.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
})
export class EventsListComponent implements OnInit {
  public seriales: Array<any>;

  constructor(
    public serialesService: SerialesService,
    public eventsService: EventsService,
    public popoverCtrl: PopoverController,
    public loader: LoaderService
  ) {
    this.seriales = this.serialesService.seriales;
    this.serialesService.$seriales.subscribe((seriales) => {
      this.seriales = seriales;
    });
  }

  ngOnInit() { }

  filterBy(serial) {
    this.loader.showLoader('Cargando eventos');
    this.eventsService.params.serial_id = serial.serial_id;
    this.eventsService.filterEventsBy().then(() => {
      this.eventsService.getAvailableMonths().then(() => {
        serial = serial.serial_id ? serial : { title: 'Todos' };
        this.serialesService.setCurrentSerial(serial);
        this.loader.dismissLoader();
        this.popoverCtrl.dismiss();
      }).catch(() => {
        this.loader.dismissLoader();
        this.popoverCtrl.dismiss();
      });
    }).catch(() => {
      this.loader.dismissLoader();
      this.popoverCtrl.dismiss();
    });
  }

}
