import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FrameItemComponent } from './frame-item.component';

describe('FrameItemComponent', () => {
  let component: FrameItemComponent;
  let fixture: ComponentFixture<FrameItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameItemComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FrameItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
