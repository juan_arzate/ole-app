import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'frame-item',
  templateUrl: './frame-item.component.html',
  styleUrls: ['./frame-item.component.scss'],
})
export class FrameItemComponent implements OnInit {
  @Input() frame: any;
  constructor() { }

  ngOnInit() { }

}
