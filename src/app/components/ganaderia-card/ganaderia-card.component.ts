import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalGanaderiaDetailsComponent } from '../modal-ganaderia-details/modal-ganaderia-details.component';
import { GanaderiasService } from 'src/app/services/ganaderias/ganaderias.service';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';

@Component({
  selector: 'ganaderia-card',
  templateUrl: './ganaderia-card.component.html',
  styleUrls: ['./ganaderia-card.component.scss'],
})
export class GanaderiaCardComponent implements OnInit {


  @Input() ganaderia: any;
  @Input() canSee: boolean = true;

  constructor(
    public modalCtrl: ModalController,
    public ganaderiaService: GanaderiasService,
    public nativePageTransitions: NativePageTransitions
  ) { }

  ngOnInit () { }

  seeGanaderia ($event) {
    if (!this.canSee) {
      return;
    }
    $event.stopPropagation();
    this.ganaderiaService.currentGanaderia = this.ganaderia;
    let $imgElement = $event.target;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalGanaderiaDetailsComponent,
      mode: 'md',
      componentProps: {
        // x: $imgElement.x,
        // y: $imgElement.y,
        // width: $imgElement.width,
        // height: $imgElement.height,
      },
    }).then((m) => {
      m.present();
    })
  }

}
