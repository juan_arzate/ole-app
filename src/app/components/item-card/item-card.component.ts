import { Component, OnInit, Input } from '@angular/core';
import { ModalItemDetailsComponent } from '../modal-item-details/modal-item-details.component';
import { ModalController } from '@ionic/angular';
import { ModalGanaderiaDetailsComponent } from '../modal-ganaderia-details/modal-ganaderia-details.component';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { TorerosService } from 'src/app/services/toreros/toreros.service';
import { ModalToreroDetailsComponent } from '../modal-torero-details/modal-torero-details.component';
import { GanaderiasService } from 'src/app/services/ganaderias/ganaderias.service';
import { ModalTorosDetailsComponent } from '../modal-toros-details/modal-toros-details.component';

@Component({
  selector: 'item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss'],
})
export class ItemCardComponent implements OnInit {

  @Input() item: any;
  @Input() source: any;
  @Input() canSee: boolean = true;

  constructor(
    public modalCtrl: ModalController,
    public torerosService: TorerosService,
    public ganaderiaService: GanaderiasService,
    public nativePageTransitions: NativePageTransitions
  ) {
  }

  ngOnInit () {
    console.log(this.item);
  }

  seeItem ($event) {
    if (!this.canSee) {
      return;
    }
    $event.stopPropagation();
    let $imgElement = $event.target;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: this.source == 'ganaderia' ? ModalGanaderiaDetailsComponent : ModalItemDetailsComponent,
      mode: 'md',
    }).then((m) => {
      m.present();
    })
  }


  seeTorero (torero) {
    this.torerosService.currentTorero = torero;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalToreroDetailsComponent,
      mode: 'md',
    }).then((m) => {
      m.present();
    })
  }


  seeToros (torero) {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalTorosDetailsComponent,
      mode: 'md',
      componentProps: {
        torero: torero,
      }
    }).then((m) => {
      m.present();
    })
  }

}
