import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalGanaderiaDetailsComponent } from './modal-ganaderia-details.component';

describe('ModalGanaderiaDetailsComponent', () => {
  let component: ModalGanaderiaDetailsComponent;
  let fixture: ComponentFixture<ModalGanaderiaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalGanaderiaDetailsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalGanaderiaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
