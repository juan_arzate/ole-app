import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ModalController, IonSlides } from '@ionic/angular';
import { GanaderiasService } from 'src/app/services/ganaderias/ganaderias.service';
import { SpacesService } from 'src/app/services/spaces/spaces.service';
import { API_URL } from 'src/app/config/config';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-modal-ganaderia-details',
  templateUrl: './modal-ganaderia-details.component.html',
  styleUrls: ['./modal-ganaderia-details.component.scss'],
})
export class ModalGanaderiaDetailsComponent implements OnInit {
  @ViewChild('cover', { static: false }) cover: ElementRef;
  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  public html: string;
  public x: any;
  public y: any;
  public width: any;
  public height: any;
  public ganaderia: any;
  public apiurl: string = API_URL;
  public slidePubOpts: any;
  public spaces: Array<any>;
  public publicidades: Array<any>;
  public loadingImage: boolean = true;

  constructor(
    public modalCtrl: ModalController,
    public renderer: Renderer2,
    public ganaderiaService: GanaderiasService,
    public spaceService: SpacesService,
    public iab: InAppBrowser,

  ) {
    this.spaces = this.spaceService.spaces;
    this.publicidades = this.spaces[5] && this.spaces[5].files.length ? this.spaces[5].files : [];
    this.spaceService.$spaces.subscribe((spaces) => {
      this.spaces = spaces;
      this.publicidades = this.spaces[5] && this.spaces[5].files.length ? this.spaces[5].files : [];
    });
    this.ganaderia = this.ganaderiaService.currentGanaderia;
  }

  ngOnInit () {
    this.slidePubOpts = {
      autoplay: {
        delay: 3000,
        disableOnInteraction: false
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          centeredSlides: false,
        },
        1023: {
          slidesPerView: 2,
          centeredSlides: false,
        },
        1366: {
          slidesPerView: 3,
          spaceBetween: 0,
          centeredSlides: false,
        }
      }
    }
  }

  ionViewDidEnter () {
    this.slides.update();
  }

  ngAfterViewInit () {
    // this.renderer.setStyle(this.cover.nativeElement, 'transition', 'all 250ms ease-in-out');
    // this.renderer.setStyle(this.cover.nativeElement, 'transform', `translate3d(0, ${this.y}px,0) scale3d(0.9,0.9,1)`);
    // this.renderer.setStyle(this.cover.nativeElement, 'z-index', `5`);

    // setTimeout(() => {
    //   this.renderer.removeStyle(this.cover.nativeElement, 'transform');
    // }, 5);
  }

  goBack () {
    this.modalCtrl.dismiss();
  }

  openBrowser (url) {
    if (!url || !this.isValidURL(url)) { return };
    this.iab.create(url, '_blank', {
      closebuttoncaption: 'Cerrar',
      closebuttoncolor: '#ffc300',
      navigationbuttoncolor: '#ffffff',
      toolbarcolor: '#571827',
      location: 'yes',
      fullscreen: 'yes',
      transitionstyle: 'fliphorizontal',
    });
  }

  isValidURL (string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
  };

}
