import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController, ToastController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';

@Component({
  selector: 'app-modal-image',
  templateUrl: './modal-image.component.html',
  styleUrls: ['./modal-image.component.scss'],
})
export class ModalImageComponent implements OnInit {

  public title: string;
  public image: any;
  public slideOpts: any;
  constructor(
    public modalCtrl: ModalController,
    public actionSheet: ActionSheetController,
    public socialSharing: SocialSharing,
    public toast: ToastController,
    public nativePageTransitions: NativePageTransitions
  ) {
    this.slideOpts = {
      zoom: {
        maxRatio: 5
      },
      allowSlidePrev: false,
      allowSlideNext: false
    }
  }

  ngOnInit () { }

  closePreview () {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.dismiss();
  }

  shareOptions () {
    let options: any = {
      message: this.title,
      chooserTitle: 'Selecciona una opción',
      url: this.image,
    }
    this.socialSharing.shareWithOptions(options).catch(() => {
      this.toast.create({
        message: 'No fue posible compartir imagen.',
        color: 'danger',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    });
  }
}
