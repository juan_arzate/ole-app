import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events/events.service';
import { GuideService } from 'src/app/services/guide/guide.service';
import { ModalImageComponent } from '../modal-image/modal-image.component';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';

@Component({
  selector: 'app-modal-item-details',
  templateUrl: './modal-item-details.component.html',
  styleUrls: ['./modal-item-details.component.scss'],
})
export class ModalItemDetailsComponent implements OnInit {
  @ViewChild('cover', { static: false }) cover: ElementRef;

  public html: string;
  public x: any;
  public y: any;
  public width: any;
  public height: any;
  public content: any;
  public source: string;
  public nombre: string;

  constructor(
    public modalCtrl: ModalController,
    public renderer: Renderer2,
    public eventService: EventsService,
    public guideService: GuideService,
    public nativePageTransitions: NativePageTransitions,
  ) {
  }

  ngOnInit () {
    if (this.source == 'event') {
      this.content = this.eventService.currentEvent.abstract;
    } else if (this.source == 'guide') {
      this.content = this.guideService.currentTopic;
    }
    this.html = this.content.contenido;
  }

  seePhoto () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalImageComponent,
      componentProps: {
        image: this.content.photo,
        title: this.nombre
      },
      cssClass: 'photo-modal'
    }).then((m) => {
      m.present();
    });
  }

  goBack () {
    this.modalCtrl.dismiss();
  }

}
