import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { ModalController, IonSlides } from '@ionic/angular';
import { TorerosService } from 'src/app/services/toreros/toreros.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SpacesService } from 'src/app/services/spaces/spaces.service';
import { API_URL } from 'src/app/config/config';

@Component({
  selector: 'app-modal-torero-details',
  templateUrl: './modal-torero-details.component.html',
  styleUrls: ['./modal-torero-details.component.scss'],
})
export class ModalToreroDetailsComponent implements OnInit {
  @ViewChild('cover', { static: false }) cover: ElementRef;
  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  public html: string;
  public x: any;
  public y: any;
  public width: any;
  public height: any;
  public apiurl: string = API_URL;
  public slidePubOpts: any;
  public torero: any;
  public spaces: Array<any>;
  public publicidades: Array<any>;

  constructor(
    public modalCtrl: ModalController,
    public renderer: Renderer2,
    public torerosService: TorerosService,
    public iab: InAppBrowser,
    public spaceService: SpacesService
  ) {
    this.torero = this.torerosService.currentTorero;
    this.spaces = this.spaceService.spaces;
    this.publicidades = this.spaces[4] && this.spaces[4].files.length ? this.spaces[4].files : [];
    this.spaceService.$spaces.subscribe((spaces) => {
      this.spaces = spaces;
      this.publicidades = this.spaces[4].files;
    });
  }

  ngOnInit () {
    this.slidePubOpts = {
      autoplay: {
        delay: 3000,
        disableOnInteraction: false
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          centeredSlides: false,
        },
        1023: {
          slidesPerView: 2,
          centeredSlides: false,
        },
        1366: {
          slidesPerView: 3,
          spaceBetween: 0,
          centeredSlides: false,
        }
      }
    }
  }

  ionViewDidEnter () {
    this.slides.update();
  }

  goBack () {
    this.modalCtrl.dismiss();
  }

  openBrowser (url) {
    if (!url || !this.isValidURL(url)) { return };
    this.iab.create(url, '_blank', {
      closebuttoncaption: 'Cerrar',
      closebuttoncolor: '#ffc300',
      navigationbuttoncolor: '#ffffff',
      toolbarcolor: '#571827',
      location: 'yes',
      fullscreen: 'yes',
      transitionstyle: 'fliphorizontal',
    });
  }

  isValidURL (string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
  };
}
