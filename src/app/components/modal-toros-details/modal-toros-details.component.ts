import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-toros-details',
  templateUrl: './modal-toros-details.component.html',
  styleUrls: ['./modal-toros-details.component.scss'],
})
export class ModalTorosDetailsComponent implements OnInit {
  public toros: any;
  public torero: any;
  constructor(
    public modalCtrl: ModalController,
  ) {
  }
  
  ngOnInit() {
    this.toros = this.torero.toros;
  }

  goBack() {
    this.modalCtrl.dismiss();
  }

}
