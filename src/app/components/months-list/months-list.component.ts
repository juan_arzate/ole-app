import { Component, OnInit } from '@angular/core';
import { EventsService } from 'src/app/services/events/events.service';
import { PopoverController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { SerialesService } from 'src/app/services/seriales/seriales.service';

@Component({
  selector: 'app-months-list',
  templateUrl: './months-list.component.html',
  styleUrls: ['./months-list.component.scss'],
})
export class MonthsListComponent implements OnInit {

  public seriales: Array<any>;
  public months: Array<any>;

  constructor(
    public eventsService: EventsService,
    public popoverCtrl: PopoverController,
    public loader: LoaderService,
    public serialesService: SerialesService,
  ) {
    this.months = this.eventsService.availableMonths;
  }

  ngOnInit() { }

  filterBy(month) {
    this.loader.showLoader('Cargando eventos');
    this.serialesService.getSeriales().then(()=>{
      this.eventsService.params.month = month;
      this.eventsService.filterEventsBy().then(() => {
        this.loader.dismissLoader();
        this.popoverCtrl.dismiss();
      }).catch(() => {
        this.loader.dismissLoader();
        this.popoverCtrl.dismiss();
      });
    }).catch(()=>{
      this.loader.dismissLoader();
      this.popoverCtrl.dismiss();
    });
  }

}
