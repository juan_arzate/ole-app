import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalToreroDetailsComponent } from '../modal-torero-details/modal-torero-details.component';
import { TorerosService } from 'src/app/services/toreros/toreros.service';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';

@Component({
  selector: 'torero-card',
  templateUrl: './torero-card.component.html',
  styleUrls: ['./torero-card.component.scss'],
})
export class ToreroCardComponent implements OnInit {

  @Input() torero: any;

  public x: any;
  public y: any;
  public width: any;
  public height: any;

  constructor(
    public modalCtrl: ModalController,
    public torerosService: TorerosService,
    public nativePageTransitions: NativePageTransitions
  ) { }

  ngOnInit () { }

  seeTorero ($event) {
    $event.stopPropagation();
    let $imgElement = $event.target;
    this.torerosService.currentTorero = this.torero;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalToreroDetailsComponent,
      mode: 'md',
      componentProps: {
        x: $imgElement.x,
        y: $imgElement.y,
        width: $imgElement.width,
        height: $imgElement.height,
      },
    }).then((m) => {
      m.present();
    })
  }

}
