import { Component, OnInit, Input } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { ModalItemDetailsComponent } from '../modal-item-details/modal-item-details.component';
import { ModalController } from '@ionic/angular';
import { GanaderiasService } from 'src/app/services/ganaderias/ganaderias.service';
import { ModalGanaderiaDetailsComponent } from '../modal-ganaderia-details/modal-ganaderia-details.component';

@Component({
  selector: 'toro-card',
  templateUrl: './toro-card.component.html',
  styleUrls: ['./toro-card.component.scss'],
})
export class ToroCardComponent implements OnInit {

  @Input() toro: any;

  constructor(
    public nativePageTransitions: NativePageTransitions,
    public ganaderiaService: GanaderiasService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit () { }

  seeDetail (detail) {
    console.log(detail);
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalItemDetailsComponent,
      mode: 'md',
      componentProps: {
        content: {
          contenido: detail.descripcion,
          photo: detail.photo,
        },
        nombre: detail.nombre
      },
    }).then((m) => {
      m.present();
    })
  }

  seeGanaderia (ganaderia) {
    this.ganaderiaService.currentGanaderia = ganaderia;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalGanaderiaDetailsComponent,
      mode: 'md',
    }).then((m) => {
      m.present();
    })
  }

}
