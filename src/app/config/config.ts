export const ID_APP = "OLE_APP";
export const SITE_URL = "https://www.oleapp.mx";
//export const SITE_URL = "http://192.168.0.28/ole";
//export const WS_URL = 'wss://tekoserver.com:1128';
export const WS_URL = 'wss://tekohost.com/wss';
export const API_URL = SITE_URL + "/api/app/";
export const AUTH_KEY = "OLE_USER_KEY";
export const FAV_EVENTS_KEY = "OLE_USER_FAV_EVENTS";
