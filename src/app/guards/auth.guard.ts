import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { UserService } from '../services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  public authorizedRole: string;

  constructor(
    public userService:UserService
  ) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    this.authorizedRole = route.data.authorizedRole;
    return this.userService.isAuthenticated() && this.authorizedRole.toLowerCase() == this.userService.user.role.toLowerCase();
  }


}
