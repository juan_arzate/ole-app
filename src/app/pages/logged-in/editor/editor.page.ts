import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { FramesService } from 'src/app/services/frames/frames.service';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { CameraService } from 'src/app/services/camera/camera.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { AlertService } from 'src/app/services/alert/alert.service';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { Base64ToGallery, Base64ToGalleryOptions } from '@ionic-native/base64-to-gallery/ngx';


@Component({
  selector: 'app-editor',
  templateUrl: './editor.page.html',
  styleUrls: ['./editor.page.scss'],
})
export class EditorPage implements OnInit {
  @ViewChild('imageCanvas', { static: false }) canvas: any;

  public frames: Array<any>;
  public slideOpts: any;
  public selectedFrame: any;
  public canvasElement: any;
  public image: any;
  public imageEditedBase64: any;
  public imageEdited: any;
  public imageEditedGalleryPath: any;
  public canvasHasLoaded: boolean = false;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public framesService: FramesService,
    public plt: Platform,
    public loader: LoaderService,
    public camera: CameraService,
    public webview: WebView,
    public socialSharing: SocialSharing,
    public file: File,
    public toast: ToastController,
    public alert: AlertService,
    public androidPermissions: AndroidPermissions,
    public base64ToGallery: Base64ToGallery
  ) {
    this.image = this.camera.currentImage;
    this.frames = this.framesService.frames;
    this.framesService.$frames.subscribe((frames) => {
      this.frames = frames;
    });
    this.loader.showLoader('Preparando editor');
    let i = setInterval(() => {
      if (this.frames && this.canvasHasLoaded) {
        this.loader.dismissLoader();
        clearInterval(i);
      }
    });
  }

  ngOnInit () {
    this.slideOpts = {
      breakpoints: {
        767: {
          slidesPerView: 3,
          spaceBetween: 0,
          centeredSlides: false,
          freeMode: true,
        },
        1023: {
          slidesPerView: 5,
          spaceBetween: 0,
          centeredSlides: false,
          freeMode: true,
        },
        1366: {
          slidesPerView: 6,
          spaceBetween: 0,
          centeredSlides: false,
          freeMode: true,
        }
      }
    }
  }

  ngAfterViewInit () {
    this.canvasElement = this.canvas.nativeElement;
    this.clear().then(() => {
      setTimeout(() => {
        this.canvasHasLoaded = true;
      }, 1500);
    });
  }

  goBack () {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.back();
  }

  selectFrame (frame) {
    console.log(frame);
    this.canvasHasLoaded = false;
    this.selectedFrame = frame;
    this.addFrame(frame.photo_path_cors);
  }

  setBackground (path): Promise<boolean> {
    return new Promise((resolve, reject) => {
      var background = new Image();
      let ctx = this.canvasElement.getContext('2d');
      ctx.ad
      background.crossOrigin = "Anonymous";
      background.onload = () => {
        ctx.drawImage(background, 0, 0, this.canvasElement.width, this.canvasElement.height);
        resolve(true);
      }
      background.src = path;
    });
  }

  addFrame (path) {
    let promise = this.setBackground("data:image/jpeg;base64," + this.image);
    promise.then(() => {
      this.loader.dismissLoader();
      var background = new Image();
      let ctx = this.canvasElement.getContext('2d');
      background.crossOrigin = "Anonymous";
      background.onload = () => {
        ctx.drawImage(background, 0, 0, this.canvasElement.width, this.canvasElement.height);
        this.canvasHasLoaded = true;
      }
      background.src = path;
    });
  }


  exportCanvasImage () {
    this.imageEditedBase64 = this.canvasElement.toDataURL();
  }

  saveToGalleryiOS () {
    let options: Base64ToGalleryOptions = { prefix: 'ole_', mediaScanner: true };
    this.base64ToGallery.base64ToGallery(this.imageEditedBase64, options).then((res) => {
      this.imageEditedGalleryPath = this.webview.convertFileSrc(res);
      this.toast.create({
        message: 'Imagen guardada en galería.',
        color: 'warning',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    }, (err) => {
      this.toast.create({
        message: 'No fue posible guardar imagen.',
        color: 'danger',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    });
  }


  saveToGallery () {
    if (this.plt.is('ios')) {
      this.saveToGalleryiOS();
    } else {
      const GALLERY_PATH = 'file:///storage/emulated/0/Download/';
      fetch(this.imageEditedBase64)
        .then(res => res.blob())
        .then(blob => {
          const UUID = 'ole-' + (new Date().getTime()).toString(16);
          this.file.checkDir(GALLERY_PATH, 'ole')
            .then(_ => {
              this.file.writeFile(GALLERY_PATH + 'ole/', UUID + '.jpg', blob).then(response => {
                this.toast.create({
                  message: 'Imagen guardada en galería.',
                  color: 'warning',
                  duration: 2000,
                  buttons: [
                    {
                      icon: 'close',
                      side: 'end',
                      text: '',
                      role: 'cancel'
                    }
                  ]
                }).then((t) => {
                  t.present();
                });
              }).catch(err => {
                console.error("Error al guardar", err);
                this.toast.create({
                  message: 'No fue posible guardar imagen.',
                  color: 'danger',
                  duration: 2000,
                  buttons: [
                    {
                      icon: 'close',
                      side: 'end',
                      text: '',
                      role: 'cancel'
                    }
                  ]
                }).then((t) => {
                  t.present();
                });
              })
            })
            .catch(err => {
              this.file.createDir(GALLERY_PATH, 'ole', false).then(result => {
                this.file.writeFile(GALLERY_PATH + 'ole/', UUID + '.jpg', blob).then(response => {
                  this.toast.create({
                    message: 'Imagen guardada en galería.',
                    color: 'warning',
                    duration: 2000,
                    buttons: [
                      {
                        icon: 'close',
                        side: 'end',
                        text: '',
                        role: 'cancel'
                      }
                    ]
                  }).then((t) => {
                    t.present();
                  });
                }).catch(err => {
                  console.error("Error al guardar", err);
                  this.toast.create({
                    message: 'No fue posible guardar imagen.',
                    color: 'danger',
                    duration: 2000,
                    buttons: [
                      {
                        icon: 'close',
                        side: 'end',
                        text: '',
                        role: 'cancel'
                      }
                    ]
                  }).then((t) => {
                    t.present();
                  });
                })
              })
            });
        });
    }
  }

  clear (event?): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (event) {
        let buttons = [
          {
            text: 'Aceptar',
            handler: () => {
              this.imageEditedBase64 = null;
              this.setBackground("data:image/jpeg;base64," + this.image).then(() => {
                resolve(true);
              });
            }
          },
          {
            text: 'Cancelar',
            role: 'cancel',
          }
        ];
        this.alert.presentAlert('¿Descartar cambios?', '', '', buttons);
      } else {
        this.imageEditedBase64 = null;
        this.setBackground("data:image/jpeg;base64," + this.image).then(() => {
          resolve(true);
        });
      }
    });
  }

  async shareFacebook () {
    this.socialSharing.shareViaFacebook('by Olé', this.imageEditedBase64).then((data) => {
      console.log(data, 'FACEBOOK');
    }).catch((err) => {
      this.toast.create({
        message: 'No se detecto Facebook en este dispositivo.',
        color: 'danger',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    });
  }

  async shareTwitter () {
    this.socialSharing.shareViaTwitter('by Olé', this.imageEditedBase64).then((data) => {
      console.log(data, 'TWITTER');
    }).catch((err) => {
      this.toast.create({
        message: 'No se detecto Twitter en este dispositivo.',
        color: 'danger',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    });
  }

  async shareInstagram () {
    this.socialSharing.shareViaInstagram('by Olé', this.imageEditedBase64).then((data) => {
      console.log(data, 'INSTAGRAM');
    }).catch((err) => {
      this.toast.create({
        message: 'No se detecto Instagram en este dispositivo.',
        color: 'danger',
        duration: 2000,
        buttons: [
          {
            icon: 'close',
            side: 'end',
            text: '',
            role: 'cancel'
          }
        ]
      }).then((t) => {
        t.present();
      });
    });
  }

}
