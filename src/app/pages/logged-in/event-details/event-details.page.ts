import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { NavController, ModalController, IonSlides } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { createAnimation } from '@ionic/core';
import { ModalItemDetailsComponent } from 'src/app/components/modal-item-details/modal-item-details.component';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { EventsService } from 'src/app/services/events/events.service';
import { UserService } from 'src/app/services/user/user.service';
import { SpacesService } from 'src/app/services/spaces/spaces.service';
import { API_URL } from 'src/app/config/config';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';


@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.page.html',
  styleUrls: ['./event-details.page.scss'],
})
export class EventDetailsPage implements OnInit {
  @ViewChild('imgCard', { 'static': false }) imgCard: any;
  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  public segmentSelected: string = 'resumen';
  public isFavorite: boolean = false;
  public initialData: any = { x: '', y: '', width: '', height: '' };
  public event: any;
  public user: any;
  public publicidades: Array<any>;
  public publicidadesGanaderias: Array<any>;
  public apiurl: string = API_URL;
  public slideOpts: any;
  public spaces: Array<any>;

  constructor(
    public userService: UserService,
    public nav: NavController,
    private route: ActivatedRoute,
    private renderer: Renderer2,
    public modalCtrl: ModalController,
    public nativePageTransitions: NativePageTransitions,
    public eventService: EventsService,
    public spaceService: SpacesService,
    public iab: InAppBrowser,

  ) {
    this.spaces = this.spaceService.spaces;
    this.publicidades = this.spaces[3].files;
    this.publicidadesGanaderias = this.spaces[5].files;
    this.spaceService.$spaces.subscribe((spaces) => {
      this.spaces = spaces;
      this.publicidades = this.spaces[3].files;
      this.publicidadesGanaderias = this.spaces[5].files;
    });

    this.user = this.userService.user;
    this.userService.$user.subscribe((user) => {
      this.user = user;
    });

    this.event = this.eventService.currentEvent;
    this.eventService.isFavorite(this.event.event_id).then((isFavorite) => {
      this.isFavorite = isFavorite;
    });

  }

  ngOnInit () {
    this.slideOpts = {
      autoplay: {
        delay: 3000,
        disableOnInteraction: false
      },
      breakpoints: {
        767: {
          slidesPerView: 1,
          centeredSlides: false,
        },
        1023: {
          slidesPerView: 2,
          centeredSlides: false,
          spaceBetween: 10,

        },
        1366: {
          slidesPerView: 3,
          spaceBetween: 10,
          centeredSlides: false,
        }
      }
    }
  }

  ionViewDidEnter () {
    this.segmentSelected = 'resumen';
    this.publicidades.sort(() => Math.random() - 0.5);
    this.publicidadesGanaderias.sort(() => Math.random() - 0.5);
    this.slides.update();
    this.slides.slideNext();

    createAnimation()
      .addElement(document.querySelector('ion-header ion-item'))
      .iterations(1)
      .duration(500)
      .delay(300)
      .direction('normal')
      .easing('ease-in-out')
      .keyframes([
        { offset: 0, top: '-100%' },
        { offset: 0, opacity: '0' },
        { offset: 1, opacity: '1' },
        { offset: 1, top: '0%' }
      ])
      .play();
    createAnimation()
      .addElement(document.querySelector('ion-segment'))
      .iterations(1)
      .duration(500)
      .delay(300)
      .direction('normal')
      .easing('ease-in-out')
      .keyframes([
        { offset: 0, top: '-100%' },
        { offset: 0, opacity: '0' },
        { offset: 1, opacity: '1' },
        { offset: 1, top: '0%' }
      ])
      .play();
  }

  segmentChanged ($event) {
    this.segmentSelected = $event.detail.value;
  }

  goBack () {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.back();
  }

  toggleFavorite (event: Event) {
    event.stopPropagation();
    if (!this.isFavorite) {
      this.eventService.saveAsFavoriteEvent(this.event.event_id).then((isFavorite) => {
        this.isFavorite = isFavorite;
      });
    } else {
      this.eventService.deleteFromFavoriteEvent(this.event.event_id).then((isFavorite) => {
        this.isFavorite = isFavorite;
      });;
    }
  }

  seeFestejo ($event) {
    $event.stopPropagation();
    let $imgElement = $event.target;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalItemDetailsComponent,
      mode: 'md',
      componentProps: {
        source: 'event',
        content: this.event.abstract
      },
    }).then((m) => {
      m.present();
    })
  }

  goToProfile () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'events', 'profile']);
  }

  getEvent (event?: any) {
    this.eventService.getEvent(this.event.event_id).then((eventDB) => {
      this.eventService.currentEvent = eventDB;
      this.event = eventDB;
      if (event) {
        event.target.disabled = true;
        event.target.complete();
        setTimeout(() => {
          event.target.disabled = false;
        }, 150);
      }
    }).catch(() => {
      if (event) {
        event.target.disabled = true;
        event.target.complete();
        setTimeout(() => {
          event.target.disabled = false;
        }, 150);
      }
    });
  }


  openBrowser (url) {
    if (!url || !this.isValidURL(url)) { return };
    this.iab.create(url, '_blank', {
      closebuttoncaption: 'Cerrar',
      closebuttoncolor: '#ffc300',
      navigationbuttoncolor: '#ffffff',
      toolbarcolor: '#571827',
      location: 'yes',
      fullscreen: 'yes',
      transitionstyle: 'fliphorizontal',
    });
  }

  isValidURL (string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
  };

}
