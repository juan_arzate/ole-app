import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController } from '@ionic/angular';
import { createAnimation } from '@ionic/core';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { EventsService } from 'src/app/services/events/events.service';
import { SerialesService } from 'src/app/services/seriales/seriales.service';
import { EventsListComponent } from 'src/app/components/events-list/events-list.component';
import { MonthsListComponent } from 'src/app/components/months-list/months-list.component';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  public events: Array<any>;
  public seriales: Array<any>;
  public serial: any;
  public user: any;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public eventService: EventsService,
    public serialService: SerialesService,
    public userService: UserService,
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit () {
    this.user = this.userService.user;
    this.userService.$user.subscribe((user) => {
      this.user = user;
    });
    this.events = this.eventService.filterEvents;
    this.eventService.$filterEvents.subscribe((events) => {
      this.events = events;
    });
    this.serial = this.serialService.currentSerial;
    this.serialService.$currentSerial.subscribe((serial) => {
      this.serial = serial;
    });
  }

  ionViewDidEnter () {
    createAnimation()
      .addElement(document.querySelector('ion-header ion-item'))
      .iterations(1)
      .duration(500)
      .delay(300)
      .direction('normal')
      .easing('ease-in-out')
      .keyframes([
        { offset: 0, opacity: '0' },
        { offset: 1, opacity: '1' }
      ])
      .play();
  }

  goToProfile () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'events', 'profile']);
  }

  openSerialesOptions () {
    this.popoverCtrl.create({
      component: EventsListComponent,
      translucent: true,
      mode: 'md',
      cssClass: 'custom-popover'
    }).then((p) => {
      p.present();
    });
  }

  openMonthsOptions () {
    this.popoverCtrl.create({
      component: MonthsListComponent,
      translucent: true,
      mode: 'md',
      cssClass: 'custom-popover'
    }).then((p) => {
      p.present();
    });
  }

}
