import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalItemDetailsComponent } from 'src/app/components/modal-item-details/modal-item-details.component';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { GuideService } from 'src/app/services/guide/guide.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.page.html',
  styleUrls: ['./guide.page.scss'],
})
export class GuidePage implements OnInit {

  public topics: Array<any>;
  public user: any;

  constructor(
    public modalCtrl: ModalController,
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public guideService: GuideService,
    public userService: UserService,
  ) {
    this.user = this.userService.user;
    this.userService.$user.subscribe((user) => {
      this.user = user;
    });
    this.topics = this.guideService.topics;
    this.guideService.$topics.subscribe((topics) => {
      this.topics = topics;
    });
  }

  ngOnInit () {
  }

  seeItem (topic) {
    this.guideService.currentTopic = topic;
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }
    this.nativePageTransitions.fade(options);
    this.modalCtrl.create({
      component: ModalItemDetailsComponent,
      mode: 'md',
      componentProps: {
        source: 'guide'
      }
    }).then((m) => {
      m.present();
    })
  }

  goToProfile () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'guide', 'profile']);
  }
}
