import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Platform, IonContent, IonSlides } from '@ionic/angular';
import { NativePageTransitions, NativeTransitionOptions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { EventsService } from 'src/app/services/events/events.service';
import { UserService } from 'src/app/services/user/user.service';
import { SpacesService } from 'src/app/services/spaces/spaces.service';
import { API_URL } from 'src/app/config/config';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('header', { static: false }) header: any;
  @ViewChild(IonContent, { static: false }) content: any;
  @ViewChild('slider', { static: false }) slides: IonSlides;

  public spaces: Array<any>;
  public user: any;
  public slideOpts: any;
  public slidePubOpts: any;
  public events: Array<any>;
  public favEvents: Array<any>;
  public publicidades: Array<any>;
  public apiurl: string = API_URL;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public eventsService: EventsService,
    public userService: UserService,
    public spaceService: SpacesService,
    public plt: Platform,
    public iab: InAppBrowser,

  ) {
    this.plt.ready().then(() => {
      this.slidePubOpts = {
        autoplay: {
          delay: 3000,
          disableOnInteraction: false
        },
        breakpoints: {
          767: {
            slidesPerView: 1,
            centeredSlides: false,
          },
          1023: {
            slidesPerView: 2,
            centeredSlides: false,
          },
          1366: {
            slidesPerView: 3,
            spaceBetween: 0,
            centeredSlides: false,
          }
        }
      };
      this.slideOpts = {
        breakpoints: {
          767: {
            slidesPerView: 1,
            spaceBetween: -100,
            centeredSlides: true,
          },
          1023: {
            slidesPerView: 2,
            spaceBetween: 0,
            centeredSlides: true,
          },
          1366: {
            slidesPerView: 3,
            spaceBetween: 0,
            centeredSlides: true,
          }
        }
      };

      this.spaces = this.spaceService.spaces;
      this.publicidades = this.spaces[2] && this.spaces[2].files.length ? this.spaces[2].files : [];
      this.spaceService.$spaces.subscribe((spaces) => {
        this.spaces = spaces;
        this.publicidades = this.spaces[2].files;
      });

      this.user = this.userService.user;
      this.userService.$user.subscribe((user) => {
        this.user = user;
      });

      this.events = this.eventsService.upcomingEvents;
      this.eventsService.$upcomingEvents.subscribe((upcomingEvents) => {
        this.events = upcomingEvents;
        let eventoInicial = this.events.findIndex((e) => {
          return e.now;
        });

        if (eventoInicial === -1) {
          eventoInicial = this.events.findIndex((e) => {
            return !e.past && !e.now;
          });
        }

        this.slideOpts.initialSlide = eventoInicial === -1 ? 0 : eventoInicial;
      });

      this.favEvents = this.eventsService.favEvents;
      this.eventsService.$favEvents.subscribe((favEvents) => {
        this.favEvents = favEvents;
      });
    });
  }

  ngOnInit() {
    let eventoInicial = -1;
    const inter = setInterval(() => {
      if (this.eventsService.eventsReady) {
        clearInterval(inter);
        eventoInicial = this.events.findIndex((e) => {
          return e.now;
        });

        if (eventoInicial === -1) {
          eventoInicial = this.events.findIndex((e) => {
            return !e.past && !e.now;
          });
        }
        this.slideOpts.initialSlide = eventoInicial === -1 ? 0 : eventoInicial;
      }

    });

  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    const i = setInterval(() => {
      if (this.content) {
        clearInterval(i);
        console.log(this.content.el.offsetWidth);
      }
    });
  }

  ionViewDidEnter() {
    this.publicidades.sort(() => Math.random() - 0.5);
    this.slides.update();
    this.slides.slideNext();
  }

  goToProfile() {
    const options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'home', 'profile']);
  }

  getEvents(event?: any) {
    this.eventsService.getEvents().then(() => {
      if (event) {
        event.target.disabled = true;
        event.target.complete();
        setTimeout(() => {
          event.target.disabled = false;
        }, 150);
      }
    }).catch(() => {
      if (event) {
        event.target.disabled = true;
        event.target.complete();
        setTimeout(() => {
          event.target.disabled = false;
        }, 150);
      }
    });
  }

  openBrowser(url) {
    if (!url || !this.isValidURL(url)) { return; }
    this.iab.create(url, '_blank', {
      closebuttoncaption: 'Cerrar',
      closebuttoncolor: '#ffc300',
      navigationbuttoncolor: '#ffffff',
      toolbarcolor: '#571827',
      location: 'yes',
      fullscreen: 'yes',
      transitionstyle: 'fliphorizontal',
    });
  }

  isValidURL(value) {
    const res = value.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null);
  }

}
