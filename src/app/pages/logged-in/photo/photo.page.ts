import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { UserService } from 'src/app/services/user/user.service';
import { CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { CameraService } from 'src/app/services/camera/camera.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.page.html',
  styleUrls: ['./photo.page.scss'],
})
export class PhotoPage implements OnInit {

  public user: any;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public userService: UserService,
    public camera: CameraService,
  ) {
    this.user = this.userService.user;
    this.userService.$user.subscribe((user) => {
      this.user = user;
    });
  }

  ngOnInit () {
  }

  goToEditor () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'photo', 'editor']);
  }

  goToProfile () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['logged-in', 'tabs', 'photo', 'profile']);
  }

  takePicture () {
    let options: CameraOptions = this.camera.defaultOptions;
    options.destinationType = this.camera.camera.DestinationType.DATA_URL;
    options.allowEdit = false;
    options.targetHeight = 1024;
    options.targetWidth = 1024;
    this.camera.takePicture(options).then((imgData) => {
      this.goToEditor();
    }).catch((error) => {
      console.log(error);
    });
  }

  fromGallery () {
    let options: CameraOptions = this.camera.defaultOptions;
    options.destinationType = this.camera.camera.DestinationType.DATA_URL;
    options.allowEdit = false;
    options.targetHeight = 1024;
    options.targetWidth = 1024;
    this.camera.fromGallery().then((imgData) => {
      this.goToEditor();
    }).catch((error) => {

    });
  }

}
