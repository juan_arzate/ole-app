import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, ToastController, ActionSheetController, Platform } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { CameraService } from 'src/app/services/camera/camera.service';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { FileTransferObject, FileTransfer, FileUploadOptions } from '@awesome-cordova-plugins/file-transfer/ngx';
import { API_URL } from 'src/app/config/config';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { AlertService } from 'src/app/services/alert/alert.service';
import { CameraOptions } from '@awesome-cordova-plugins/camera/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public user: any;
  public cameraOptions: CameraOptions;
  public uploaderOptions: FileUploadOptions;
  public fileTransfer: FileTransferObject;

  constructor(
    public nav: NavController,
    public userService: UserService,
    public nativePageTransitions: NativePageTransitions,
    public popoverCtrl: PopoverController,
    public toast: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public transfer: FileTransfer,
    public file: File,
    public camera: CameraService,
    public loader: LoaderService,
    public alertCtrl: AlertService,
    public platform: Platform
  ) {
    this.user = this.userService.user;
    this.userService.$user.subscribe((user) => {
      this.user = user;
    });
  }

  ngOnInit () {
  }

  goBack () {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.back();
  }

  logOut () {
    this.userService.logout();
  }

  toggleNotification (source, toggle: boolean) {
    let notification: any = {};
    if (source == 'app') {
      notification = {
        id: this.userService.user.id,
        notification_app: +(!toggle),
      }
    } else if (source == 'email') {
      notification = {
        id: this.userService.user.id,
        notification_email: +(!toggle),
      }
    }
    let buttons = [
      {
        text: 'Aceptar',
        cssClass: 'secondary',
        handler: () => {
          this.userService.edit(notification).then(() => {
            this.toast.create({
              message: `Notificaciones ${source == 'app' ? 'en app' : 'por correo electrónico'} han sido ${source == 'app' ? (notification.notification_app ? 'activadas' : 'desactivadas') : (notification.notification_email ? 'activadas' : 'desactivadas')}`,
              color: 'warning',
              duration: 2000,
              position: 'top',
              buttons: [
                {
                  role: 'cancel',
                  text: '',
                  icon: 'close',
                  side: 'end'
                }
              ]
            }).then((t) => {
              t.present();
            });
          }).catch(() => {
            this.toast.create({
              message: 'Algo ha salido mal. Vuelve a intentarlo porfavor.',
              color: 'danger',
              duration: 2000,
              position: 'top',
              buttons: [
                {
                  role: 'cancel',
                  text: '',
                  icon: 'close',
                  side: 'end'
                }
              ]
            }).then((t) => {
              t.present();
            });
          });
        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'danger',
        handler: () => {
          if (source == 'app') {
            this.user.notification_app = !this.user.notification_app;
          } else {
            this.user.notification_email = !this.user.notification_email;
          }
        }
      }
    ];
    let header = !toggle ? 'ACTIVAR' : 'DESACTIVAR';
    let subHeader = !toggle ? `¿Deseas activar notificaciones ${source == 'app' ? 'en aplicación' : 'por email'}?` : `¿Deseas desactivar notificaciones ${source == 'app' ? 'en aplicación' : 'por email'}?`;
    this.alertCtrl.presentAlert(header, subHeader, '', buttons);
  }

  async presentActionSheet () {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Cambiar foto de perfil',
      cssClass: 'custom-actionsheet',
      buttons: [{
        text: 'Tomar foto',
        handler: () => {
          this.takePicture();
        }
      }, {
        text: 'Desde galería',
        handler: () => {
          this.fromGallery();
        }
      }, {
        text: 'Cancelar',
        role: 'cancel',
      }]
    });
    await actionSheet.present();
  }

  takePicture () {
    if (this.platform.is('cordova')) {
      let options: CameraOptions = this.camera.defaultOptions;
      options.destinationType = this.camera.camera.DestinationType.FILE_URI;
      options.allowEdit = true;
      options.targetHeight = 1024;
      options.targetWidth = 1024;
      this.camera.takePicture(options).then((imgData) => {
        this.loader.showLoader('Actualizando foto de perfil');
        this.uploadHandler(imgData).then((res) => {
          this.userService.edit({ photo: res.response, id: this.userService.user.id }).then(() => {
            setTimeout(() => {
              this.loader.dismissLoader();
              this.popoverCtrl.dismiss();
            }, 1500);
          });
        }).catch((error) => {
          setTimeout(() => {
            this.loader.dismissLoader();
            this.popoverCtrl.dismiss();
          }, 1500);
        });
      }).catch(() => {
        this.popoverCtrl.dismiss();
      });
    }
  }

  fromGallery () {
    if (this.platform.is('cordova')) {
      let options: CameraOptions = this.camera.defaultOptions;
      options.destinationType = this.camera.camera.DestinationType.FILE_URI;
      options.allowEdit = true;
      options.targetHeight = 1024;
      options.targetWidth = 1024;
      this.camera.fromGallery(options).then((imgData) => {
        this.loader.showLoader('Actualizando foto de perfil');
        this.uploadHandler(imgData).then((res) => {
          this.userService.edit({ photo: res.response, id: this.userService.user.id }).then(() => {
            setTimeout(() => {
              this.loader.dismissLoader();
              this.popoverCtrl.dismiss();
            }, 1500);
          });
        }).catch((error) => {
          setTimeout(() => {
            this.loader.dismissLoader();
            this.popoverCtrl.dismiss();
          }, 1500);
        });
      }).catch(() => {
        this.popoverCtrl.dismiss();
      });
    }
  }

  uploadHandler (filePath): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fileTransfer = this.transfer.create();
      this.uploaderOptions = {
        fileKey: 'file',
        fileName: filePath.substr(filePath.lastIndexOf('/') + 1),
      }
      console.log(this.uploaderOptions, filePath);
      this.fileTransfer.upload(filePath, API_URL + 'files/file', this.uploaderOptions).then((res) => {
        console.log(res);
        resolve(res);
      }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  }
}
