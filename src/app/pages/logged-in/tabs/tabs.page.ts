import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public isHome: boolean = true;
  public isEvents: boolean = false;
  public isPhoto: boolean = false;
  public isGuide: boolean = false;

  constructor() {
  }

  setOnIcon(tab) {
    this.isHome = (tab == 'home');
    this.isEvents = (tab == 'events');
    this.isPhoto = (tab == 'photo');
    this.isGuide = (tab == 'guide');
  }

  resetIcon() {
    this.isHome = false;
    this.isEvents = false;
    this.isPhoto = false;
    this.isGuide = false;
  }
}
