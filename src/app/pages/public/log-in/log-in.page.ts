import { Component, OnInit, APP_ID } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
// tslint:disable-next-line:max-line-length
import { SignInWithApple, AppleSignInResponse, AppleSignInErrorResponse, ASAuthorizationAppleIDRequest } from '@awesome-cordova-plugins/sign-in-with-apple/ngx';
import jwt_decode from 'jwt-decode';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { SpacesService } from 'src/app/services/spaces/spaces.service';
import { AUTH_KEY } from 'src/app/config/config';
import { FacebookService } from 'src/app/services/facebook/facebook.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.page.html',
  styleUrls: ['./log-in.page.scss'],
})
export class LogInPage implements OnInit {
  public email: string;
  public password: string;
  public time = 0;
  public formGroup: FormGroup;
  public fotoPatrocinador: string;

  constructor(
    public nav: NavController,
    public userService: UserService,
    public nativePageTransitions: NativePageTransitions,
    public formBuilder: FormBuilder,
    public loader: LoaderService,
    public alert: AlertService,
    public plt: Platform,
    public spaceService: SpacesService,
    public fbService: FacebookService,
    public storage: Storage,
    public signInWithApple: SignInWithApple
  ) {

    this.formGroup = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.plt.ready().then(() => {
      const interval = setInterval(() => {
        this.time += 100;
        if (this.time === 1500) {
          clearInterval(interval);
        }
      }, 100);
    });
  }


  login() {
    if (this.formGroup.valid) {
      this.loader.showLoader('Iniciando sesión');
      this.userService.login(this.formGroup.value).then(() => {
        this.loader.dismissLoader();
      }).catch((message) => {
        this.loader.dismissLoader();
        this.alert.presentAlert('¡Oops!', '', message, [{ text: 'OK', role: 'cancel' }]);
      });
    } else {
      this.formGroup.get('email').markAsTouched();
      this.formGroup.get('password').markAsTouched();
    }
  }

  goToRegister() {
    const options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    this.nativePageTransitions.fade(options);
    this.nav.navigateForward(['register']);
  }

  logInByFacebook() {
    this.fbService.logInFacebook().then((userFB) => {
      this.loader.showLoader('');
      this.userService.getUser({ email: userFB.email }).then((userDB) => {
        this.userService.edit({ photo: userFB.picture.data.url, id: userDB.id }).then(() => {
          this.storage.set(AUTH_KEY, JSON.stringify({ id: userDB.id, role: userDB.role, facebook: true })).then(() => {
            this.loader.dismissLoader();
            this.userService.isLogged.next(true);
          });
        });
      }).catch((message) => {
        const newUser = {
          email: userFB.email,
          password: APP_ID + userFB.email
        };
        this.userService.register(newUser).then((userR) => {
          this.userService.edit({ photo: userFB.picture.data.url, id: userR.id }).then(() => {
            this.storage.get(AUTH_KEY).then((userS) => {
              if (userS) {
                const userStorage = JSON.parse(userS);
                userStorage.facebook = true;
                this.storage.set(AUTH_KEY, JSON.stringify(userStorage));
              } else {
                this.storage.set(AUTH_KEY, JSON.stringify({ id: userR.id, role: userR.role, facebook: true }));
              }
              this.loader.dismissLoader();
            });
          });
        }).catch((error) => {
          this.loader.dismissLoader();
          this.alert.presentAlert('¡Oops!', '', error, [{ text: 'OK', role: 'cancel' }]);
        });
      });
    }).catch((e) => {
      console.log(e, 'error');
      // tslint:disable-next-line:max-line-length
      this.alert.presentAlert('¡Oops!', 'No se logró la conexión con Facebook: ' + e.errorMessage, 'Intenta de nuevo por favor.', [{ text: 'OK', role: 'cancel' }]);
    });
  }


  logInByApple() {
    this.signInWithApple.signin({
      requestedScopes: [
        ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
        ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail
      ]
    }).then((userApple: AppleSignInResponse) => {
      if (!userApple.email) {
        const tokenData = jwt_decode(userApple.identityToken);
        // @ts-ignore
        userApple.email = tokenData.email;
      }
      this.loader.showLoader('');
      this.userService.getUser({ email: userApple.email }).then((userDB) => {
        this.storage.set(AUTH_KEY, JSON.stringify({ id: userDB.id, role: userDB.role, apple: true })).then(() => {
          this.loader.dismissLoader();
          this.userService.isLogged.next(true);
        });
      }).catch((message) => {
        const newUser = {
          email: userApple.email,
          password: APP_ID + userApple.email
        };
        this.userService.register(newUser).then((userR) => {
            this.storage.get(AUTH_KEY).then((userS) => {
              if (userS) {
                const userStorage = JSON.parse(userS);
                userStorage.apple = true;
                this.storage.set(AUTH_KEY, JSON.stringify(userStorage));
              } else {
                this.storage.set(AUTH_KEY, JSON.stringify({ id: userR.id, role: userR.role, apple: true }));
              }
              this.loader.dismissLoader();
            });
        }).catch((error) => {
          this.loader.dismissLoader();
          this.alert.presentAlert('¡Oops!', '', error, [{ text: 'OK', role: 'cancel' }]);
        });
      });
    }).catch((e: AppleSignInErrorResponse) => {
      console.log(e, 'error');
      // tslint:disable-next-line:max-line-length
      this.alert.presentAlert('¡Oops!', 'No se logró la conexión con Apple: ' + e.localizedDescription, 'Intenta de nuevo por favor.', [{ text: 'OK', role: 'cancel' }]);
    });
  }


}
