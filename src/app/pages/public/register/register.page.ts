import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NativeTransitionOptions, NativePageTransitions } from '@awesome-cordova-plugins/native-page-transitions/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { FacebookService } from 'src/app/services/facebook/facebook.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SITE_URL } from '../../../config/config';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public formGroup: FormGroup;
  public site_url: string = SITE_URL;

  constructor(
    public nav: NavController,
    public nativePageTransitions: NativePageTransitions,
    public formBuilder: FormBuilder,
    public userService: UserService,
    public loader: LoaderService,
    public alert: AlertService,
    public fbService: FacebookService,
    public iab: InAppBrowser,
  ) {
    this.formGroup = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
      passwordR: ['', Validators.compose([Validators.required])],
      terms: [false, Validators.requiredTrue]
    });

    this.formGroup.valueChanges.subscribe((group) => {
      if (group.password != group.passwordR) {
        this.formGroup.get('passwordR').setErrors({ isNotEqual: true });
      } else {
        if (this.formGroup.get('passwordR').hasError('isNotEqual')) {
          this.formGroup.get('passwordR').setErrors({ isNotEqual: null });
        }
      }
    });

  }

  ngOnInit () {
  }

  goBack () {
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this.nativePageTransitions.fade(options);
    this.nav.back();
  }

  register () {
    if (this.formGroup.valid) {
      this.loader.showLoader('Espera por favor.');
      this.userService.register(this.formGroup.value).then(() => {
        this.loader.dismissLoader();
      }).catch((message) => {
        this.loader.dismissLoader();
        this.alert.presentAlert('¡Oops!', '', message, [{ text: 'OK', role: 'cancel' }]);
      });
    } else {
      this.formGroup.get('email').markAsTouched();
      this.formGroup.get('password').markAsTouched();
      this.formGroup.get('passwordR').markAsTouched();
      this.formGroup.get('terms').markAsTouched();
    }
  }

  openBrowser (url) {
    if (!url || !this.isValidURL(url)) { return };
    this.iab.create(url, '_blank', {
      closebuttoncaption: 'Cerrar',
      navigationbuttoncolor: '#ffffff',
      toolbarcolor: '#571827',
      closebuttoncolor: '#ffc300',
      location: 'yes',
      fullscreen: 'yes',
      transitionstyle: 'fliphorizontal',
    });
  }

  isValidURL (string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
  };
}
