import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AlertButton, AlertInput } from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public isActive: boolean;

  constructor(
    public alertController: AlertController,
  ) { }

  async presentAlert(header: string = '', subHeader: string = '', message: string = '', buttons?: Array<AlertButton>, inputs?: Array<AlertInput>) {
    const alert = await this.alertController.create({
      backdropDismiss:false,
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: buttons,
      inputs: inputs,
      cssClass:'frego-alert'
    });
    
    alert.onDidDismiss().then(()=>{
      this.isActive = false;
    });

    await alert.present().then(()=>{
      this.isActive = true;
    });
  }
}
