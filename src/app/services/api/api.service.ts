import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from '../../config/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'

})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(action): Observable<any> {
    return this.http.get(API_URL + action);
  }

  post(action, params,nested = false): Observable<any> {
    const DATA = new HttpParams({
      fromObject: nested ? {data:JSON.stringify(params)} : params,
    });

    return this.http.post(API_URL + action, DATA);
  }
}
