import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { LoaderService } from '../loader/loader.service';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  public defaultOptions: CameraOptions;
  public currentImage: any;

  constructor(
    public camera: Camera,
    public loader: LoaderService
  ) {
    this.defaultOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
  }

  takePicture (options: CameraOptions = this.defaultOptions): Promise<any> {
    options.sourceType = this.camera.PictureSourceType.CAMERA;
    this.loader.showLoader('Abriendo cámara');
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((data) => {
        this.currentImage = data;
        resolve(data);
        //this.loader.dismissLoader();
      }).catch((error) => {
        reject(error);
        //this.loader.dismissLoader();
      });
    });
  }

  fromGallery (options: CameraOptions = this.defaultOptions): Promise<any> {
    options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.loader.showLoader('Abriendo galería');
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((data) => {
        this.currentImage = data;
        //this.loader.dismissLoader();
        resolve(data);
      }).catch((error) => {
        //this.loader.dismissLoader();
        reject(error);
      });
    });
  }
}
