import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject } from 'rxjs';
import { FAV_EVENTS_KEY } from 'src/app/config/config';
import { Storage } from '@ionic/storage';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  public currentEvent: any;
  public favEvents: Array<any>;
  public $favEvents: Subject<any> = new Subject();
  public upcomingEvents: Array<any>;
  public $upcomingEvents: Subject<any> = new Subject();
  public filterEvents: Array<any>;
  public $filterEvents: Subject<any> = new Subject();
  public availableMonths: Array<any>;
  public params: any;
  public eventsReady: boolean = false;

  constructor(
    public api: ApiService,
    public storage: Storage,
    public userService: UserService,
  ) {
    this.params = {
      serial_id: 0,
      month: 0
    }

    let promiseEvents = this.getEvents();
    let promiseFavoriteEvents = this.getFavoriteEvents();
    let promiseMonths = this.getAvailableMonths();
    let promiseFilterEvents = this.filterEventsBy();

    Promise.all([promiseEvents,promiseFavoriteEvents,promiseMonths,promiseFilterEvents]).then(()=>{
      this.eventsReady = true;
    });
  }

  getEvent(event_id) {
    return new Promise((resolve, reject) => {
      this.api.get(`events/event/${event_id}`).subscribe((res) => {
        if (res.hasOwnProperty('error')) {
          if (res.error === true) {
            reject();
          } else {
            this.currentEvent = res.event;
            this.currentEvent.date.replace(/-/g, "/");
            resolve(res.event);
          }
        } else {
          reject('Ocurrió un problema durante el proceso.');
        }
      }, (error) => {
        reject('No se logró realizar solicitud.');
      });
    });
  }

  getEvents(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('events/events', this.params).subscribe((res) => {
        this.upcomingEvents = res.events;
        this.upcomingEvents.forEach((e) => {
          e.date.replace(/-/g, "/");
        });
        this.$upcomingEvents.next(this.upcomingEvents);
        resolve(res.events);
      }, (error) => {
        this.upcomingEvents = [];
        this.$upcomingEvents.next(this.upcomingEvents);
        reject('No se logró realizar solicitud.');
      });
    });
  }

  filterEventsBy(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('events/events', this.params).subscribe((res) => {
        this.filterEvents = res.events;
        this.filterEvents.forEach((e) => {
          e.date.replace(/-/g, "/");
        });
        this.$filterEvents.next(this.filterEvents);
        resolve(res.events);
      }, (error) => {
        this.filterEvents = [];
        this.$filterEvents.next(this.filterEvents);
        reject('No se logró realizar solicitud.');
      });
    });
  }

  getAvailableMonths(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('events/months', this.params).subscribe((res) => {
        this.availableMonths = res.months;
        this.availableMonths = this.availableMonths.filter((m) => {
          return m != 0;
        });
        resolve();
      }, (error) => {
        this.availableMonths = [];
        reject('No se logró realizar solicitud.');
      });
    });
  }

  getFavoriteEvents(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get(FAV_EVENTS_KEY).then((userFavEvents) => {
        if (userFavEvents) {
          let data = {
            in: userFavEvents
          }
          this.api.post('events/events', data).subscribe((res) => {
            this.favEvents = res.events;
            this.favEvents.forEach((e) => {
              e.date.replace(/-/g, "/");
            });
            this.$favEvents.next(this.favEvents);
            resolve();
          }, (error) => {
            reject('No se logró realizar solicitud.');
          });
        } else {
          this.favEvents = [];
          this.$favEvents.next(this.favEvents);
          resolve();
        }
      });
    });
  }

  saveAsFavoriteEvent(event_id): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.get(FAV_EVENTS_KEY).then((userFavEvents) => {
        if (userFavEvents) {
          let favEvents = JSON.parse(userFavEvents);
          let event = favEvents.find((e) => {
            return e == event_id;
          });
          if (!event) {
            this.api.post('events/addAsFavorite', { user_id: this.userService.user.id, event_id: event_id }).subscribe((res) => {
              if (!res.error) {
                favEvents.push(event_id);
                this.storage.set(FAV_EVENTS_KEY, JSON.stringify(favEvents)).then(() => {
                  console.log(favEvents);
                  resolve(true);
                });
              } else {
                resolve(false);
              }
            })
          } else {
            resolve(true);
          }
        } else {
          this.storage.set(FAV_EVENTS_KEY, JSON.stringify([event_id])).then(() => {
            resolve(true);
          });
        }
      });
    });
  }

  deleteFromFavoriteEvent(event_id): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.get(FAV_EVENTS_KEY).then((userFavEvents) => {
        if (userFavEvents) {
          this.api.post('events/addAsFavorite', { user_id: this.userService.user.id, event_id: event_id }).subscribe((res) => {
            if (!res.error) {
              let favEvents = JSON.parse(userFavEvents);
              favEvents = favEvents.filter((e) => {
                return e != event_id;
              });
              this.storage.set(FAV_EVENTS_KEY, JSON.stringify(favEvents)).then(() => {
                console.log(favEvents);
                resolve(true);
              });
            } else {
              resolve(false);
            }
          });           
        }
      }).catch(()=>{
        resolve(false);
      });
    });
  }

  isFavorite(event_id): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.get(FAV_EVENTS_KEY).then((userFavEvents) => {
        if (userFavEvents) {
          let favEvents = JSON.parse(userFavEvents);
          let event = favEvents.find((e) => {
            return (e == event_id);
          });
          resolve(event ? true : false);
        } else {
          resolve(false);
        }
      }).catch(() => {
        resolve(false);
      });
    });
  }

}
