import { Injectable } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@awesome-cordova-plugins/facebook/ngx';

@Injectable({
  providedIn: 'root'
})
export class FacebookService {

  constructor(public fb: Facebook) { }


  logInFacebook (): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
        this.getUserInfo(res.authResponse.userID).then((res) => {
          resolve(res);
        }).catch((e) => {
          console.log(e);
          reject(e);
        });
      }).catch((e) => {
        console.log(e);
        reject(e);
      });
    });
  }

  getUserInfo (userId: string): Promise<any> {
    return this.fb.api('me?fields=' + ['name', 'email', 'first_name', 'last_name', 'picture.type(large)'].join(), null)
  }

  logOutFacebook (): Promise<any> {
    return this.fb.logout();
  }
}
