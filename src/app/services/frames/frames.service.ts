import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FramesService {
  public frames: Array<any>;
  public $frames: Subject<any> = new Subject();

  constructor(
    public api: ApiService,
  ) {
    this.frames = [];
    this.getFrames();
  }

  getFrames(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('frames/get').subscribe((res) => {
        this.frames = res.frames;
        this.$frames.next(this.frames);
        resolve(res.frames);
      }, (error) => {
        this.frames = [];
        this.$frames.next(this.frames);
        reject();
      });
    });
  }
}
