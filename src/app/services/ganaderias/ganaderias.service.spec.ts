import { TestBed } from '@angular/core/testing';

import { GanaderiasService } from './ganaderias.service';

describe('GanaderiasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GanaderiasService = TestBed.get(GanaderiasService);
    expect(service).toBeTruthy();
  });
});
