import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GuideService {

  public currentTopic: any;
  public topics: Array<any>;
  public $topics: Subject<any> = new Subject();

  constructor(
    public api: ApiService,
  ) {
    this.topics = [];
    this.getTopics();
  }

  getTopics(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('guide/get').subscribe((res) => {
        this.topics = res.topics;
        this.$topics.next(this.topics);
        resolve();
      }, (error) => {
        this.topics = [];
        this.$topics.next(this.topics);
        reject();
      });
    });
  }

}
