import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public isLoadingSpinner = false;

  constructor(
    public loadingController: LoadingController,
  ) { }

  async showLoader(mensaje: string, spinner: any = 'crescent', translucent: boolean = true) {
    await this.dismissLoader();
    await this.loadingController
      .create(
        {
          message: mensaje ? mensaje : '',
          spinner: spinner,
          translucent: translucent,
          backdropDismiss: false,
          showBackdrop: true,
          cssClass: 'loader-custom'
        }
      )
      .then(res => {
        res.present();
      });
  }

  async dismissLoader() {
    while (await this.loadingController.getTop() !== undefined) {
      await this.loadingController.dismiss();
    }
  }

}