import { TestBed } from '@angular/core/testing';

import { SerialesService } from './seriales.service';

describe('SerialesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SerialesService = TestBed.get(SerialesService);
    expect(service).toBeTruthy();
  });
});
