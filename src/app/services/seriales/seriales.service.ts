import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SerialesService {

  public $seriales: Subject<any> = new Subject();
  public seriales: Array<any>;
  public currentSerial: any;
  public $currentSerial: Subject<any> = new Subject();;

  constructor(
    public api: ApiService,
  ) {

    this.getSeriales();
  }

  getSeriales(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('seriales/get', {}).subscribe((res) => {
        this.seriales = res.seriales;
        this.$seriales.next(this.seriales);
        resolve();
      }, (error) => {
        this.seriales = [];
        this.$seriales.next(this.seriales);
        resolve();
      });
    });
  }

  setCurrentSerial(serial) {
    this.currentSerial = serial;
    this.$currentSerial.next(serial);
  }

}
