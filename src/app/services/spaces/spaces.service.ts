import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpacesService {

  public spacesReady: boolean = false;
  public spaces: Array<any>;
  public currentSplash: any;
  public $spaces: Subject<any> = new Subject();

  constructor(
    public api: ApiService,
  ) {
    this.spaces = [];
    this.getSpaces().then(()=>{
      this.currentSplash = (this.spaces.length && this.spaces[0].files.length)? this.spaces[0].files[Math.floor(Math.random() * this.spaces[0].files.length)] : {path:"assets/splash.png"};
      this.spacesReady = true;
      console.log(this.currentSplash);
    }).catch(()=>{
      this.spacesReady = true;
      this.currentSplash = {path:"assets/splash.png"};
    });
  }

  getSpaces(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.get('publicity/get').subscribe((res) => {
        this.spaces = res.pages;
        this.$spaces.next(this.spaces);
        resolve(res.pages);
      }, (error) => {
        reject(error);
        this.spaces = [];
        this.$spaces.next(this.spaces);
      });
    });
  }
}
