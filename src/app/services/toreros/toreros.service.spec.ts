import { TestBed } from '@angular/core/testing';

import { TorerosService } from './toreros.service';

describe('TorerosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TorerosService = TestBed.get(TorerosService);
    expect(service).toBeTruthy();
  });
});
