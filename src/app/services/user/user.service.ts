import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { AUTH_KEY } from 'src/app/config/config';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { FacebookService } from '../facebook/facebook.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public isLogged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user: any;
  public $user: Subject<any> = new Subject();

  constructor(
    public storage: Storage,
    public api: ApiService,
    public plt: Platform,
    public fbService: FacebookService,
  ) {
    this.checkUser();
  }

  login(data): Promise<any> {

    return new Promise((resolve, reject) => {
      this.api.post('users/login', data).subscribe((res: any) => {
        if (res.hasOwnProperty('error')) {
          if (res.error === true) {
            this.isLogged.next(false);
            reject(res.message);
          } else {
            this.storage.set(AUTH_KEY, JSON.stringify({ id: res.user.id, role: res.user.role })).then(() => {
              this.user = { id: res.user.id, role: res.user.role };
              this.getUser({ id: res.user.id}).then((userDB) => {
                this.user = userDB;
                this.isLogged.next(true);
                resolve();
              });
            });
          }
        } else {
          reject('Ocurrió un problema durante el inicio de sesión. Intenta de nuevo por favor.');
        }
      }, (error) => {
        reject('Algo salió mal, no se logró conexión con el servidor.');
      });

    });
  }

  getUser(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('users/get-user', data).subscribe((res) => {
        if (res.hasOwnProperty('error')) {
          if (res.error === true) {
            reject(res.message);
          } else {
            this.user = res.user;
            this.$user.next(this.user);
            resolve(res.user);
          }
        } else {
          reject('Ocurrió un problema durante el inicio de sesión. Intenta de nuevo por favor.');
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  logout() {
    this.storage.get(AUTH_KEY).then((user)=>{
      user = JSON.parse(user);
      if(user){
        if(user.facebook){
          this.fbService.logOutFacebook().then(()=>{
            this.storage.remove(AUTH_KEY).then(() => {
              this.isLogged.next(false);
            });
          }).catch((error)=>{
            console.log(error);
          });
        }else{
          this.storage.remove(AUTH_KEY).then(() => {
            this.isLogged.next(false);
          });
        }
      }else{
        this.isLogged.next(false);
      }
    });
  }

  isAuthenticated() {
    return this.isLogged.value;
  }

  checkUser() {
    return this.storage.get(AUTH_KEY).then((user) => {
      if (user) {
        this.user = JSON.parse(user);
        this.getUser({id:this.user.id}).then((userDB) => {
          this.user = userDB;
          this.isLogged.next(true);
        });
      }
    });
  }

  register(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('users/register', data, true).subscribe((res: any) => {
        if (res.hasOwnProperty('error')) {
          if (res.error === true) {
            this.isLogged.next(false);
            reject(res.message);
          } else {
            this.storage.set(AUTH_KEY, JSON.stringify({ id: res.user.id, role: res.user.role })).then(() => {
              this.getUser({id:res.user.id}).then((userDB) => {
                this.user = userDB;
                this.isLogged.next(true);
                resolve(userDB);
              });
            });
          }
        } else {
          reject('Vaya vaya!, problemas durante el registro. Intenta de nuevo por favor.');
        }
      }, (error) => {
        reject('Puede que no tengas conexión de internet. Verifica tu conexión por favor.');
      });
    });
  }

  edit(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.post('users/edit', data, true).subscribe((res: any) => {
        if (res.hasOwnProperty('error')) {
          if (res.error === true) {
            reject(res.message);
          } else {
            this.getUser({id:data.id}).then((userDB) => {
              this.user = userDB;
              resolve(res.message);
            });
          }
        } else {
          reject('Vaya vaya!, problemas durante la actualización de tus datos. Intenta de nuevo por favor.');
        }
      }, (error) => {
        reject('Puede que no tengas buena conexión de internet. Verifica tu conexión por favor.');
      });
    });
  }

  
}
