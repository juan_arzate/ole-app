import { TestBed } from '@angular/core/testing';

import { WssSocketService } from './wss-socket.service';

describe('WssSocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WssSocketService = TestBed.get(WssSocketService);
    expect(service).toBeTruthy();
  });
});
