import { Injectable } from '@angular/core';
import { WS_URL } from 'src/app/config/config';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class WssSocketService {

  private ws: WebSocket;
  private onMessage: Function;
  public connected$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    public plt: Platform
  ) {

    this.plt.ready().then(() => {
      this.onMessage = (message) => { console.log(message) };
      this.connected$.subscribe((connected) => {
        if (!connected) {
          this.initWS();
        }
      });
    });

  }

  initWS() {
    if(!this.connected$.value){
      this.ws = new WebSocket(WS_URL);
      this.ws.onopen = () => {
        console.log('[ WEB SOCKET SERVICE WORKING! ]');
        this.connected$.next(true);
      };
  
      this.ws.onerror = () => {
        this.ws.close();
        this.connected$.next(false);
      };
  
      this.ws.onmessage = (message) => {
        this.getMessage(message);
      }

      this.ws.onclose = () => {
        console.log('[ WEB SOCKET SERVICE CLOSED! ]');
      }
    }
  }

  getMessage(messageEv: MessageEvent) {
    let message = JSON.parse(messageEv.data);
    this.onMessage(message);
  }

  sendMessage(message) {
    if (this.connected$.value) {
      this.ws.send(JSON.stringify(message));
    }
  }

  setOnMessage(handler:Function){
    this.ws.close();
    this.onMessage = handler;
    this.connected$.next(false);
  }

}
